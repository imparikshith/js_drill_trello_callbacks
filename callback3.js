const fs = require("node:fs");
function callback3(listId, callback) {
  setTimeout(() => {
    fs.readFile("../cards.json", (error, data) => {
      if (error) {
        callback("Error in reading the file", null);
      } else {
        try {
          const cards = JSON.parse(data);
          let cardIdsAndDescription;
          for (let list in cards) {
            if (list === listId) {
              cardIdsAndDescription = cards[list];
              break;
            }
          }
          if (cardIdsAndDescription) {
            callback(null, cardIdsAndDescription);
          } else {
            callback("Cards does not exist for the given listID", null);
          }
        } catch (error) {
          callback(error, null);
        }
      }
    });
  }, 2000);
}
module.exports = callback3;