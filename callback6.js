const fs = require("node:fs");
const callback2 = require("./callback2.js");
const callback3 = require("./callback3.js");
function callback6() {
  setTimeout(() => {
    fs.readFile("../boards.json", "utf-8", (error, data) => {
      if (error) {
        console.log(error);
      } else {
        try {
          const board = JSON.parse(data).find(
            (board) => board.name === "Thanos"
          );
          console.log(board);
          setTimeout(() => {
            callback2(board.id, (error, data) => {
              if (error) {
                console.log(error);
              } else {
                console.log(data);
                setTimeout(() => {
                  for (let list of data) {
                    callback3(list.id, (error, data) => {
                      if (error) {
                        console.log(error);
                      } else {
                        console.log(data);
                      }
                    });
                  }
                }, 2000);
              }
            });
          }, 2000);
        } catch (error) {}
      }
    });
  }, 2000);
}
module.exports = callback6;