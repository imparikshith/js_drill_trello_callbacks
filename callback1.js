const fs = require("node:fs");
function callback1(boardId, callback) {
  setTimeout(() => {
    fs.readFile("../boards.json", "utf-8", (error, data) => {
      if (error) {
        callback("Error in reading the file", null);
      } else {
        try {
          const boards = JSON.parse(data);
          const board = boards.find((board) => board.id === boardId);
          if (board) {
            callback(null, board);
          } else {
            callback("Board not found", null);
          }
        } catch (error) {
          callback(error, null);
        }
      }
    });
  }, 2000);
}
module.exports = callback1;