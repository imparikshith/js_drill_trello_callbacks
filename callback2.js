const fs = require("node:fs");
function callback2(boardID, callback) {
  setTimeout(() => {
    fs.readFile("../lists_1.json", "utf-8", (error, data) => {
      if (error) {
        callback("Error in reading the file", null);
      } else {
        try {
          const lists = JSON.parse(data);
          let listsFromBoardId;
          for (let board in lists) {
            if (board === boardID) {
              listsFromBoardId = lists[board];
              break;
            }
          }
          if (listsFromBoardId) {
            callback(null, listsFromBoardId);
          } else {
            callback("Lists does not exist for the given boardID", null);
          }
        } catch (error) {
          callback(error, null);
        }
      }
    });
  }, 2000);
}
module.exports = callback2;